## 项目信息

**项目名称：** Integrate With OpenTelemetry

**方案描述：**

- Metrics：使用OpenTelemetry对EventMesh中的metrics进行export，使用Prometheus进行数据的接收，可视化
- Trace：
  - HTTP：可以通过OpenTelemetry的Java instrumentation进行自动追踪
  - TCP：埋点和Javaagent支持

**时间规划：**

- **已完成：**
  - 7.1~7.12：了解eventMesh和openTelemetry的metrics如何导出
  - 7.13~7.20：尝试着进行HTTP的metrics的导出
  - 7.21~7.30：完善了HTTP的metrics的导出，添加了tcp的metrics导出
  - 8.1~8.6：完成了所有metrics的导出
  - 8.7~8.15：了解trace如何实现，了解Java agent机制，了解Java instrumentation，学习如何埋点
  
- **待完成：**
  - 8.15~8.30：完成HTTPtrace的埋点
  - 8.31~9.30：完成链路中不同协议的trace

## 项目进度

####   启动eventMesh时遇到的问题以及解决的办法

- 安装rocketMQ环境遇到的问题：

  因为我是在window上安装的，然后官网的文档看不太懂，配置了环境变量以后，不知道怎么启动。

  然后在csdn上面找到了window安装rocketMQ的教程（[RocketMQ 在windows下基本使用_bzqgo的博客-CSDN博客](https://blog.csdn.net/bzqgo/article/details/110187736)）

  顺着教程又遇到了问题，在启动boker的时候出现找不到主类的错误，又继续找原因，在另外一篇csdn上面找到了原因（[rocketmq broker启动报错，找不到或无法加载主类_路过君的博客-CSDN博客_rocketmq启动broker未找到类](https://blog.csdn.net/zhoudingding/article/details/107927657)）

- 什么是spi机制？

  在阅读quickstart时看到的

  ![image-20210704142831181](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210704142831181.png)

  搜索了资料，阅读后了解到是一种可热拔插机制

#### 阅读open telemetry的收获

![image-20210705214502042](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210705214502042.png)

Open Telemetry 定义了三种*公制工具*：

- `counter`：一个随时间累加的值——你可以把它想象成汽车上的里程表；它只会上升。

- `measure`：随着时间的推移聚合的值。这更类似于汽车上的行程里程表，它代表某个定义范围内的值。

- `observer`：捕获特定时间点的一组当前值，例如车辆中的燃油表。

  我使用的都是observer，对metrics进行定期捕获

###### SDK解读

**这里是Open Telemetry中Prometheus的example，这个example主要思路是：**

有3个值incomingMessageCount1，2，3每秒钟变成一个100以内的随机值

然后otle创建一个meter，用这个meter来observer这三个值，meter就会自动定期导出三个值到19090端口

然后我设置了Prometheus每3秒钟读取一次导出到19090的数据

![image-20210811130919776](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210811130919776.png)

```java
public final class ExampleConfiguration {
  private static HTTPServer server;//这里是导出data的http服务，端口是19090

  /**
   * Initializes the Meter SDK and configures the prometheus collector with all default settings.
   * 这个方法会获得一个meterProvider同时把http服务启动
   * @param prometheusPort the port to open up for scraping.
   * @return A MeterProvider for use in instrumentation.
   */
  static MeterProvider initializeOpenTelemetry(int prometheusPort) throws IOException {
    SdkMeterProvider meterProvider = SdkMeterProvider.builder().buildAndRegisterGlobal();

    PrometheusCollector.builder().setMetricProducer(meterProvider).buildAndRegister();

    server = new HTTPServer(prometheusPort);//使用非守护线程启动一个 HTTP 服务器，为默认的 Prometheus 注册表提供服务。

    return meterProvider;
  }

  static void shutdownPrometheusEndpoint() {//停止服务
    server.stop();
  }
}
```

---

```java
//这里原来只是观察incomingMessageCount一个变量，23都是我加上去的，测试如何同时观察多个metrics
public final class PrometheusExample {
  //这些都是观察的目标
  private long incomingMessageCount;
  private long incomingMessageCount2;
  private long incomingMessageCount3;

  public PrometheusExample(MeterProvider meterProvider) {
    Meter meter = meterProvider.get("PrometheusExample", "0.13.1");//第一个参数是仪器名，第二个是仪器版本
    meter
        .longValueObserverBuilder("incoming.messages")//这个是用来查找的名字，这个指标的名字，'.'要改成'_'
        .setDescription("No of incoming messages awaiting processing")//对这个指标的描述
        .setUnit("message")//设置单位
        //下面就是一个observer，对incomingMessageCount进行observer，然后key：value的label，label也可以为空
        .setUpdater(result -> result.observe(incomingMessageCount, Labels.of("a","one")))
        .build();
      //同上
     meter
        .longValueObserverBuilder("incoming.messages2")
        .setDescription("No of incoming messages awaiting processing")
        .setUnit("message")
        .setUpdater(result -> result.observe(incomingMessageCount2, Labels.of("b","two")))
        .build();
     meter
        .longValueObserverBuilder("incoming.messages3")
        .setDescription("No of incoming messages awaiting processing")
        .setUnit("message")
        .setUpdater(result -> result.observe(incomingMessageCount3, Labels.of("c","three")))
        .build();

  }

  void simulate() {//改变incomingMessageCount的值
    for (int i = 180; i > 0; i--) {
      try {
        System.out.println(
            i + " Iterations to go, current incomingMessageCount is:  " + incomingMessageCount);
        incomingMessageCount = ThreadLocalRandom.current().nextLong(100);
        incomingMessageCount2 = ThreadLocalRandom.current().nextLong(100);
        incomingMessageCount3 = ThreadLocalRandom.current().nextLong(100);
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        // ignored here
      }
    }
  }

  public static void main(String[] args) throws IOException {
    int prometheusPort = 0;
    try {
      prometheusPort = Integer.parseInt(args[0]);
    } catch (Exception e) {
      System.out.println("Port not set, or is invalid. Exiting");
      System.exit(1);
    }//这里主要是传入端口

    // it is important to initialize the OpenTelemetry SDK as early as possible in your process.
    MeterProvider meterProvider = ExampleConfiguration.initializeOpenTelemetry(prometheusPort);

    PrometheusExample prometheusExample = new PrometheusExample(meterProvider);

    prometheusExample.simulate();

    System.out.println("Exiting");

    // clean up the prometheus endpoint
    ExampleConfiguration.shutdownPrometheusEndpoint();
  }
}
```

#### 写metrics代码的思路

我的思路其实就是模仿上面example的思路

- 首先是导出的HTTP端口，一开始我是写死代码的，然后提交到GitHub也会社区的前辈们提出要写在配置文件里面，我也请教了然后写在了配置文件里面，所以要改变导出数据的端口要修改配置文件里面的数据

- 另外还有就是Prometheus的配置，要设置从上面导出的端口中读取数据。例如上面导出端口是19090，我就要在prometheus.yml中加这个以下内容：

  ![image-20210811132853730](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210811132853730.png)

- 然后模仿otle的example写HTTP server，然后我设计的是唯一的一个端口导出数据，也就是http和tcp的metrics都是导出到这个地方，http和tcp也都是用同一个meter，我就把公共的部分提到一个类里面OpenTelemetryExporterConfiguration

- 这个类里面我希望server和meterProvider是唯一的，所以用static修饰

  ![image-20210811133417484](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210811133417484.png)

  类里面主要的方法就是

  - **init方法：**先判断server启动了吗，没启动的话，启动server，然后创建meterProvider；启动了的话，返回已经启动的meterProvider
  - **start方法：**对目标变量进行observer
  - **stop方法：**停止server

- 在分别写OpenTelemetryHTTPMetricsExporter和OpenTelemetryTCPMetricsExporter

- 这两个方法主要是传入目标变量所在的类给OpenTelemetryExporterConfiguration，然后启动observer

- 然后分别在eventmeshHTTPserver和tcpservser的start方法添加startopen telemetry的方法

#### 对trace的理解

阅读Google Dapper：Dapper, a Large-Scale Distributed Systems Tracing Infrastructure论文以后获得的理解：

以CS模式为例，client发起一个请求以后，就会创建一个span，这个是根span

span有id，name等信息

span会记录每个事务的开始和结束时间

请求传输的过程中会有子span的产生

子span还会有parent id，通过parent id可以知道传播的路径

根span记录的是整个请求花费的时间

子span记录的是在某个节点，或者某个事务花费的时间

请求到服务器之前经历的节点或事务会创建span，这些是子span，

这些信息收集导出后就可以知道从client发出来的请求经历了哪些节点和事务，以及耗费的时间

数据是收集有带内传播和带外传播，每个apm都会有性能损耗的报告

## Metrics的Prometheus展示

下面是我写好的代码启动quick start后将导出的metrics在Prometheus中的展示：

![image-20210813124912340](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210813124912340.png)

![image-20210813124946748](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210813124946748.png)

![image-20210813125354905](C:\Users\86130\AppData\Roaming\Typora\typora-user-images\image-20210813125354905.png)

## 项目心得

- 首先非常感谢soc提供了这么一个机会可以让我参与到开源项目，我也是从一个小白不停的在学习，第一次了解到大家都是在GitHub这种在平台上讨论项目的，我也学会了提issue，提pr，导师和社区的前辈都给予了我巨大的帮助。
- 很多以前在学校完全没听说过的东西，像rocketmq，Kafka，Prometheus......太多太多的技术，还有像git和docker这些我只在b站上面自学过，但用起来才发现还是需要有人指点才行。
- 还有阅读代码的时候，许多的builder，provider绕来绕去，你调用了我，我又调用你，让我头疼不已，后面在b站上学了一下23种设计模式才知道这原来是建造者模式，是链式编程，是lambda表达式，再回去看代码就有一种恍然大悟的感觉，不再需要绕来绕去。

## 后续的安排

后续要继续学习rpc框架，学习埋点，完成tcp和http的trace，完成agent。
